# http://www.babynamesdirect.com/indian-baby-names/Girl/R/2

import requests
import collections
import csv
from bs4 import BeautifulSoup

base_url = "https://www.babynamesdirect.com/baby-names/indian/girl/s/"

baby_names = list()

for i in range(0, 10):
    url = base_url
    if i > 0:
        url += str(i)
    print(url)
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    lis = soup.find_all('li', {"class": "ntr"})
    # print(lis)
    for li in lis:
        names = li.find_all('a')
        for name in names:
            # print(name.text)
            baby_names.append(name.text)
    i += 1

baby_names.sort()
numerolgy_keyset = {"aijqy": "1", "brk": "2", "cgls": "3", "dmt": "4", "ehnx": "5", "uvw": "6", "oz": "7", "fp": "8"}
name_to_numerology = collections.OrderedDict()
print(baby_names)

for nom in baby_names:
    val = 0
    for char in nom.lower():
        for key in numerolgy_keyset:
            if char in key:
                val += int(numerolgy_keyset[key])
    if val in [23,32,41,14,50,5,6,15,51,24,42,33,60]:
        name_to_numerology[nom] = val

print(name_to_numerology)

with open('baby_names.csv', 'w') as f:
    w = csv.writer(f)
    w.writerows(name_to_numerology.items())
