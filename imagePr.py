import matplotlib.pyplot as plt
import os
import pylab
import cv2
from skimage import io, color

os.chdir(r'/Volumes/Official/Installation/ZROP')

rgb = io.imread('TAn7D.jpg')
lab = color.rgb2lab(rgb)

L, a, b = cv2.split(lab)
x1=a>10
x2=a<40
x3=b>10
x4=b<70
img=x1 & x2 & x3 & x4
plt.figure(1)
plt.subplot(1, 3, 1), plt.imshow(rgb)
plt.title('Original')
plt.subplot(1, 3, 2), plt.imshow(img, cmap='Greys')
plt.title('Greyscale')
plt.subplot(1, 3, 3), plt.imshow(img, cmap='Greys')
plt.title('Binary')
plt.show()

# pylab.imshow(thresh1)
# pylab.show()
# pylab.imshow(img, cmap='Greys')
# pylab.show()