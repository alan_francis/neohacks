import requests
import pandas
from bs4 import BeautifulSoup

url = 'http://hydromet.lcra.org/repstage.asp'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')
tables = soup.find_all('table')

# convert the html table data into pandas data frames, skip the heading so that it is easier to add a column
df = pandas.read_html(str(tables[1]), skiprows={0}, flavor="bs4")[0]

# loop over the table to find out station id and store it in a dict obj
a_links = soup.find_all('a', attrs={'class': 'tablink'})
stnid_dict = {}
for a_link in a_links:
    cid = ((a_link['href'].split("dataWin('STAGE','"))[1].split("','")[0])
    stnid_dict[a_link.text] = cid

# add the station id column from the stnid_dict object above
df.loc[:, (len(df.columns)+1)] = df.loc[:, 0].apply(lambda x: stnid_dict[x])
df.columns = ['Station', 'Time', 'Stage', 'Flow', 'StationID']

# added custom order of columns to add in csv, and to skip row numbers in the output file
df.to_csv('station.csv', columns=['Station', 'StationID', 'Time', 'Stage', 'Flow'], index=False)
