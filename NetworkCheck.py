import requests
import time
import datetime
import sys
import logging

test_url = "https://www.google.com"
frequency = 10

if len(sys.argv) > 1:
    test_url = sys.argv[1]
    if len(sys.argv) > 2:
        frequency = int(sys.argv[2])

logger = logging.getLogger('NetworkCheck')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(message)s')
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)
fh = logging.FileHandler(filename='NetworkCheckLog_'+'{:%Y-%b-%d_%H_%M_%S}'.format(datetime.datetime.now())+'.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.info('logging requests for the url: ' + test_url)
logger.info('Request frequency: ' + str(frequency))
while True:
    try:
        start_time = time.time()
        response = requests.get(test_url)
        resp_text = response.content
        response_time = time.time() - start_time
        # print('[{:%Y-%b-%d %H:%M:%S}]'.format(datetime.datetime.now()), "Response time:", response_time, 'seconds')
        logger.info(" Response time: " + str(response_time) + ' seconds')
    except requests.exceptions.RequestException as error:
        # print('[{:%Y-%b-%d %H:%M:%S}]'.format(datetime.datetime.now()), "Connection Error")
        logger.warning('Connection Error')
        logger.exception(error)
    time.sleep(frequency)
