# https://www.kaggle.com/titericz/results

import requests
import json

jsonResponse = requests.get("https://www.kaggle.com/knockout/profiles/54836/results")
data = json.loads(jsonResponse.text)
print(data)

for eachData in data:
    print("competition name:", eachData["competition"]["title"])
    print("Rank:", eachData["rank"])
    print("competitors count:", eachData["teamCount"])

