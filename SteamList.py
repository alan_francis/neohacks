# http://store.steampowered.com/genre/Free%20to%20Play/?tab=MostPlayed#p2

# http://store.steampowered.com/search/tabpaginated/render/?query=&start=10&count=10&genre=37&tab=MostPlayed&cc=IN&l=english

import requests
import json
from bs4 import BeautifulSoup
import re

for i in range(0, 32):
    start_count = i * 10;
    jsonResponse = requests.get("http://store.steampowered.com/search/tabpaginated/render/"
                                "?query=&start="+str(start_count)+"&count=10&genre=37&tab=MostPlayed&cc=IN&l=english")
    data = json.loads(jsonResponse.text)
    soup = BeautifulSoup(data["results_html"], "html.parser")
    alltitles = soup.find_all(attrs={'class': re.compile('tab_item_name')})
    for title in alltitles:
        print(title.text)
