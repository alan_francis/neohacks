
# rename files in order
# logic : torename.txt in the folder
# skip that file while reading all the visible files in a folder, and rename in alphabetical order.

import os

root_folder = '/Volumes/Personal/Media/Parks and Recreations/Season 7/'

with open(root_folder+'torename.txt') as f:
    new_file_names = f.readlines()

print(new_file_names)
i = 0
for root, dirs, files in os.walk(root_folder):
    for name in files:
        if name in ['torename.txt', 'toappend.txt', '.DS_Store']:
            continue
        old_file = os.path.join(root, name)
        fname, extn = os.path.splitext(old_file)
        new_name = os.path.join(root, new_file_names[i].replace('\n', '') + extn)
        # new_name = os.path.join(root, fname + new_file_names[i].replace('\n', '') + extn) # for toappend.txt
        print(old_file, '--->', new_name)
        os.rename(old_file, new_name)
        i += 1
