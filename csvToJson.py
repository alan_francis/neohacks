import json
from collections import OrderedDict


def list_to_csv(listdat):
    csv = ""
    for val in listdat:
        csv = csv+","+str(val)
    return csv[1:]

lines = []
csvfile = "csvfile.csv"
outcsvfile = "outcsvfile.csv"
jsonfile = "jsonfile.json"

with open(csvfile, encoding='UTF-8') as a_file:
        for line in a_file:
            lines.append(line.strip())

columns = lines[0].split(",")
data = lines[1:]

whole_data = []
for row in data:
    fields = row.split(",")
    i = 0
    rowData = OrderedDict()
    for column in columns:
        rowData[columns[i]] = fields[i]
        i += 1
    whole_data.append(rowData)

with open(jsonfile) as json_file:
    jsondata = json.load(json_file)

keys = list(jsondata.keys())

for key in keys:
    value = jsondata[key]
    for each_row in whole_data:
        each_row[key] = value

with open(outcsvfile, mode='w', encoding='UTF-8') as b_file:
    b_file.write(list_to_csv(columns)+'\n')
    for row_data in whole_data:
        row_list = []
        for ecolumn in columns:
            row_list.append(row_data.get(ecolumn))
        b_file.write(list_to_csv(row_list)+'\n')

