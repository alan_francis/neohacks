__author__ = 'alan-1554'

from bs4 import BeautifulSoup
import requests
import os

# root_url = 'https://forums.manageengine.com/servicedesk-plus/query-report-requests-module/'
# root_url = 'https://forums.manageengine.com/servicedesk-plus/query-report-tasks-module'
root_url = 'https://forums.manageengine.com/servicedesk-plus/query-report-change-module'
root_folder = '/Volumes/Official/Stuff/ZHacks'
location = root_folder + '/' + 'sdp-forum'
progressFile = location + '/' + 'reports_changes.txt'
delim = '\t'
if not os.path.exists(location):
    os.makedirs(location)


page = requests.get(root_url)
soup = BeautifulSoup(page.text, 'html.parser')
divs = soup.find_all('div', class_="ndflLeft width89")
for eachDiv in divs:
    print(eachDiv.h3.a['title'], delim, eachDiv.h3.a['href'])
    with open(progressFile, mode='a', encoding='UTF-8') as b_file:
        b_file.write(eachDiv.h3.a['title'] + delim + eachDiv.h3.a['href']+'\n')

i = 2
while i < 55:
    page = requests.get(root_url+'/'+str(i))
    i += 1
    soup = BeautifulSoup(page.text, 'html.parser')
    divs = soup.find_all('div', class_="ndflLeft width89")
    if len(divs) < 1:
        exit(1)

    for eachDiv in divs:
        print(eachDiv.h3.a['title'], delim, eachDiv.h3.a['href'])
        with open(progressFile, mode='a', encoding='UTF-8') as b_file:
            b_file.write(eachDiv.h3.a['title'] + delim + eachDiv.h3.a['href']+'\n')

