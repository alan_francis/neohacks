import requests
import json
import csv
from requests_toolbelt.multipart.encoder import MultipartEncoder

url = "http://it360-w7-lap5:9191/sdpapi/change"
url_params = {"TECHNICIAN_KEY": "F5ECB3D9-C096-4738-98BC-28B3501A8E59",
              "OPERATION_NAME": "GET_ALL",
              "format": "json"}

jsonString = requests.get(url, params=url_params).text
respJson = json.loads(jsonString)
status = respJson["operation"]["result"]["status"]
message = respJson["operation"]["result"]["message"]
print(message)
if status == "Success" and message != "No Changes present":
    changes = respJson["operation"]["details"]
    columns = changes[0].keys()
    print('Columns to upload: ', columns)
    print('No of rows to upload: ', len(changes))

    with open('upload.csv', 'w') as out_file:
        writer = csv.DictWriter(out_file, fieldnames=columns)  # https://docs.python.org/3.5/library/csv.html
        writer.writeheader()
        writer.writerows(changes)

    zemail = "alan.francis@zohocorp.com"
    zdb = "MESDP_1"
    ztable = "Change"
    reportsapi = "https://alan-1554:8443/api/" + zemail + "/" + zdb + "/" + ztable
    zr_params = {"ZOHO_ACTION": "IMPORT",
                 "authtoken": "b329903cc609a2133e3bd0e867f683b0",
                 # https://accounts.zoho.com/apiauthtoken/create?SCOPE=ZohoReports/reportsapi
                 "ZOHO_OUTPUT_FORMAT": "JSON",
                 "ZOHO_ERROR_FORMAT": "JSON",
                 "ZOHO_API_VERSION": "1.0",
                 "ZOHO_CREATE_TABLE": "true",
                 "ZOHO_IMPORT_TYPE": "TRUNCATEADD",
                 "ZOHO_AUTO_IDENTIFY": "true",
                 "ZOHO_ON_IMPORT_ERROR": "SKIPROW"}

    m = MultipartEncoder(
            fields={'ZOHO_FILE': ('filename', open('upload.csv', 'rb'), 'text/plain')}
    )

    zr_header = {"Content-Type": m.content_type}

    postresponse = requests.post(reportsapi, params=zr_params, data=m, headers=zr_header, verify=False)

    print(json.loads(postresponse.text))
else:
    print('Error in fetching data from SDP')