import urllib.request
from bs4 import BeautifulSoup


def crawlForData(url):
        r = urllib.request.urlopen(url)
        soup = BeautifulSoup(r.read(),'html.parser')
        result = [i.text.replace('\n', ' ').strip() for i in soup.find_all('p')]
        for p in result:
                print(p)

url = 'https://en.wikipedia.org/wiki/Adivasi'
crawlForData(url)
