
from bs4 import BeautifulSoup

html = """
<div class="result" style="width:100%;">
  <span class="uppercase bold country-name" style="width:100%;">
        Russia
        <span class="city-names">
         St. Petersburg
        </span>
  </span>
  <br/>
  <span class="bold">
    <a href="http://eap.ucop.edu/OurPrograms/russia/Pages/russian_area_studies_st_petersburg.aspx" target="_blank">
        Russian Area Studies, St. Petersburg - Fall
    </a>
  </span>
  <br/>Council on International Educational Exchange, St. Petersburg
  <br/>St. Petersburg State University
</div>
"""

soup = BeautifulSoup(html, "html.parser")
cities = soup.find_all('div', attrs={'class': 'result'})
for city in cities:
    spans = city.find_all('span')
    for span in spans:
        span.decompose()
    text_you_need = BeautifulSoup(str(city),"html.parser").find('div').text
    university = text_you_need.strip().split('\n')[1].strip()
    print(university)
