import requests
from bs4 import BeautifulSoup

url = 'https://www.airbnb.com/rooms/5711344'
html = requests.get(url)
soup = BeautifulSoup(html.text, 'html.parser')
divs = soup.find_all('div', attrs={'class': 'col-md-3 text-muted'})
for div in divs:
    space = str(div.find('span').text.strip())
    if space == "The Space":
        print(space)
