# http://www.prokerala.com/kids/baby-names/hindu/girl/

import requests
import collections
import csv
from bs4 import BeautifulSoup

base_url = "http://www.prokerala.com/kids/baby-names/hindu/boy/k/"

baby_names = list()

for i in range(0, 56):
    url = base_url
    if i > 0:
        url = url + 'page-' + str(i) + '.html'
    # print(url)
    response = requests.get(url)
    # print(response.text)
    soup = BeautifulSoup(response.text, 'html.parser')
    trs = soup.find_all('tr')
    for tr in trs:
        tds = tr.find_all('td')
        bname = ''
        for td in tds:
            names = td.find_all('a')
            for name in names:
                # print(name.text)
                bname = name.text
                baby_names.append(name.text)

        if len(tds) > 0 and len(bname) > 0:
            print(bname, ',', tds[1].text)
    i += 1

