from bs4 import BeautifulSoup
import requests


def get_new_cnn(url):
        response = requests.get(url, headers={'User-agent': 'Mozilla/5.0'})
        soup = BeautifulSoup(response.text, 'html.parser')
        # 1) Link to the website
        print(url)
        # 2) Date article published
        date = soup.find("p", attrs={"class": "update-time"})
        print(date.text.replace('Updated ', ''))
        # 3) title of article
        title = soup.find("h1", attrs={"class": "pg-headline"})
        print(title.text.encode('UTF-8'))
        # 4) Text of the article
        paragraphs = soup.find_all('p', attrs={"class": "zn-body__paragraph"})
        text = " ".join([paragraph.text for paragraph in paragraphs])
        print(text.encode('UTF-8'))

src_url = 'http://www.cnn.com/2013/10/29/us/florida-shooting-cell-phone-blocks-bullet/index.html?hpt=ju_c2'
get_new_cnn(src_url)
