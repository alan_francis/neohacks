from bs4 import BeautifulSoup
html = """<div style="display: flex">
            <div class="half" style="font-size: 0.8em;width: 33%;"> apple </div>
            <div class="half" style="font-size: 0.8em;text-align: center;width: 28%;"> peach </div>
            <div class="half" style="font-size: 0.8em;text-align: right;width: 33%;" title="nofruit"> cucumber </div>
        </div>"""

soup = BeautifulSoup(html, "html.parser")
divs = soup.find_all('div', attrs={'style': 'font-size: 0.8em;text-align: center;width: 28%;', 'class': 'half'})
for div in divs:
    print(div.text)
