from urllib.request import urlopen
from bs4 import BeautifulSoup
import pandas

url = 'http://www.basketball-reference.com/teams/ATL/2016.html'
html = urlopen(url)
soup = BeautifulSoup(html, 'html.parser')

advs = soup.find('table', attrs={'id': 'advanced'})
table = pandas.read_html(str(advs), flavor="bs4")
print(table)
