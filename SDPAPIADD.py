import requests
import json
import random

host = "192.168.167.253"
port = "8080"
protocol = "http"
url = protocol + "://" + host + ":" + port
urlparams = {"OPERATION_NAME": "ADD", "format": "json", "TECHNICIAN_KEY": "F7A65BFB-4567-416C-878A-5933F5B9A4AE"}
details = {
    'title': '',
    'description': '',
    'templatename': '',
    'workflowname': '',
    'stagename': '',
    'statusname': '',
    'requestedby': '',
    'changemanager': '',
    'technician': '',
    'category': '',
    'subcategory': '',
    'item': '',
    'risk': '',
    'urgency': '',
    'priority': '',
    'changetype': '',
    'reasonforchange': '',
    'sitename': '',
    'impact': '',
    'statuscomments': '',
    'servicesaffected': '',
    'assets': '',
    'scheduledstarttime': '',
    'scheduledendtime': ''
}
titles = ["New change from python", "New change from API", "Tokyo Ghoul Re:", "Business as Usual"]
template = ["General Template", "Emergency Template"]
stage = ["Submission", "Planning", "Approval", "Implementation", "Review", "Close"]
status = ["Requested", "Accepted", "Rejected", "Requested for Information", "Submitted for Authorization"]
change_requester = ["Alan", "Frank", "Paco", "Max", "Frieda", "Lizzy"]
change_tech = ["Alan", "Heather Graham", "Howard Stern", "Jennifer Doe"]
category = ["Desktop Hardware", "General", "Internet", "Network", "Operating System", "Printers", "Routers", "Services", "Software", "Switches", "Telephone", "User Administration"]
subcategory = ["Adobe Reader", "IE7", ".Net Framework", "MS Office", "Servicedesk Plus", "Symantec Antivirus Software"]
item = ["Install", "Uninstall", "Repair Installation", "Configuration", "Grant Access", "License Extension"]
risk = ["High", "Low", "Medium"]
urgency = ["High", "Low", "Normal", "Urgent"]
priority = ["High", "Low", "Medium", "Normal"]
change_type = ["Major", "Minor", "Significant", "Standard"]
impact = ["high", "low", "medium"]
group = ["Hardware Problems", "Network", "Printer Problems"]

for x in range(1, 100):
    details["title"] = random.choice(titles)
    details["templatename"] = random.choice(template)
    details["stagename"] = random.choice(stage)
    details["statusname"] = random.choice(status)
    details["requestedby"] = random.choice(change_requester)
    details["technician"] = random.choice(change_tech)
    details["category"] = random.choice(category)
    details["subcategory"] = random.choice(subcategory)
    details["item"] = random.choice(item)
    details["risk"] = random.choice(risk)
    details["urgency"] = random.choice(urgency)
    details["priority"] = random.choice(priority)
    details["changetype"] = random.choice(change_type)
    details["impact"] = random.choice(impact)

    post_data = {
        'INPUT_DATA': '{"operation": {"details": '+json.dumps(details)+'}}'}
    response = requests.post(url + '/sdpapi/change', params=urlparams, data=post_data)
    json_response = json.loads(response.text)
    if json_response['operation']['result']['status'] == 'Success':
        print('Change Added, Change ID:', json_response['operation']['details'][0]['CHANGEID'])
    else:
        print('Change Add Request Failed', post_data)
    x += 1
