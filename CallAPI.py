import requests
import json
import random
import sys

json_file_name = 'addchange.json'
if len(sys.argv) > 1:
    json_file_name = sys.argv[1]

with open(json_file_name) as json_file:
    jsondata = json.load(json_file)

url = jsondata["protocol"] + "://" + jsondata["host"] + ":" + jsondata["port"] + "/" + jsondata["url"]
count = jsondata["count"]
url_params = jsondata["url_params"]
data = jsondata["data"]
print('API to call:', url)
print('Repeat Count:', count)

details = {}
for x in range(0, int(count)):
    for key in data:
        if len(data[key]) > 0:
            details[key] = random.choice(data[key])

    print('Data sent:', details)
    post_data = {
        'INPUT_DATA': '{"operation": {"details": '+json.dumps(details)+'}}'}
    response = requests.post(url, params=url_params, data=post_data)
    if 'Success' not in response.text:
        print(response.text)
    else:
        print('API Call Success, Response:', response.text)
    x += 1
