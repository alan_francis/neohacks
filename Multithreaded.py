import csv
import requests
from multiprocessing import Pool
import datetime


def write_to_file(txt, filename, prefix):
    with open(filename, mode='a') as fw:
        fw.write(prefix+ str(txt) + '\n')
        fw.flush()


def write_to_outfile(txt):
    write_to_file(txt, filename='csvoutput.csv', prefix='prefix - ')

with open('csvinput.csv', mode='rb') as reader:
    lines = reader.readlines()
    lines.pop(0)
    print(lines)
    pool = Pool(10)
    pool.map(write_to_outfile, lines)

